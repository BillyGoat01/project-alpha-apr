from django.shortcuts import render, redirect

from projects.form import ProjectForm

from projects.models import Project

from django.contrib.auth.decorators import login_required


@login_required
def project_lists(request):
    project = Project.objects.filter(members=request.user)
    context = {"project": project}
    return render(request, "projects/projects_list.html", context)


@login_required
def project_detail(request, pk):
    project_deets = Project.objects.get(pk=pk)
    context = {"project_deets": project_deets}
    return render(request, "projects/projects_detail.html", context)


@login_required
def project_create(request):
    context = {}
    form = ProjectForm(request.POST or None)
    if form.is_valid():
        project = form.save()
        return redirect("show_project", pk=project.pk)
    context["form"] = form
    return render(request, "projects/projects_create.html", context)
