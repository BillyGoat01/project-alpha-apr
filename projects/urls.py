from django.urls import path

from projects.views import project_create, project_lists, project_detail

urlpatterns = [
    path("", project_lists, name="list_projects"),
    path("<int:pk>/", project_detail, name="show_project"),
    path("create/", project_create, name="create_project"),
]
