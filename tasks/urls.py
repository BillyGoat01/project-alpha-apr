from django.urls import path

from tasks.views import Task_lists, task_create, complete_task

urlpatterns = [
    path("create/", task_create, name="create_task"),
    path("mine/", Task_lists, name="show_my_tasks"),
    path("<int:pk>/complete/", complete_task, name="complete_task"),
]
