from django.shortcuts import render, redirect, get_object_or_404

from django.contrib.auth.decorators import login_required

from tasks.form import TaskForm, CompleteTaskForm

from tasks.models import Task


@login_required
def task_create(request):
    context = {}
    form = TaskForm(request.POST or None)
    if form.is_valid():
        task = form.save()
        return redirect("show_project", pk=task.pk)
    context["form"] = form
    return render(request, "tasks/tasks_create.html", context)


@login_required
def Task_lists(request):
    task_list = Task.objects.filter(assignee=request.user)
    context = {"task_list": task_list}
    return render(request, "tasks/tasks_list.html", context)


@login_required
def complete_task(request, pk):
    context = {}
    obj = get_object_or_404(Task, pk=pk)
    form = CompleteTaskForm(request.POST or None, instance=obj)
    if form.is_valid():
        form.save()
        return redirect("show_my_tasks")
    context["form"] = form
    return render(request, "tasks/tasks_list.html", context)
